# The MIT License (MIT)

# Copyright (c) 2015 Alexis Marquet

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

#run with python 3.4 with twisted, midiport, and pygame installed


import http.client
from xml.dom.minidom import parse,parseString;
import xml.dom.minidom;
import twisted
from twisted.internet import reactor,task;
import threading;
import rtmidi;
import random;
import time;
import sys,os;
import socket;
import pygame;
from pygame.locals import *
import signal
global notetab
global departureList
global refresh_running

coordinates = [[46.225344, 6.080102],[46.174729, 6.184590]]
dim = [1440,850]
do =    48+0
dod =   48+1
re =    48+2
red =   48+3
mi =    48+4
fa =    48+5
fad =   48+6
sol =   48+7
sold =  48+8
la =    48+9
lad =   48+10
si =    48+11
do2 =   48+12

dorienne = [do,re,red,fa,sol,la,lad,do+12,re+12,red+12,fa+12,sol+12,la+12,lad+12,do2+12]
partons = [do,re,mi,fad,sold,lad,do2,re+12,mi+12,fad+12,sold+12,lad+12,do2+12]
pentatonique = [dod,red,fad,sold,lad,dod+12,red+12,fad+12,sold+12,lad+12,dod+24]
scales = [dorienne,partons,pentatonique]

for item in scales:
    print(len(item))
    if len(item)%2 == 0:
        print('all scales must be of odd length')
        sys.exit(-1);

scales_names = ['dorienne','par tons','pentatonique']

scale = []
#global next_scale_change
next_scale_change = 0.0

#stop_numbers = open('departureCodes.txt','w')
#time_length = open('length.txt','w')
note_lock = [0]*128
dot = pygame.image.load('dot.bmp')
dot = pygame.transform.scale(dot, (10, 10))
carte = pygame.image.load('map.bmp')
carte = pygame.transform.scale(carte,(dim[0],dim[1]))
colorList = {}



def switch_scale():
    global next_scale_change
    global scales
    global scale
    i = random.randint(0,len(scales)-1)
    print(i)
    scale = scales[i]
    print("==========================changed scale,",scales_names[i])
 
class BusStopped:
    def __init__(self,linecode,departureCode,delay,stop,midiport):
        self.departureCode = departureCode
        self.stop = stop
        self.midiport = midiport
        self.linecode = linecode
        self.eventID = reactor.callLater(delay,noteOn,self.linecode,self.departureCode,self.stop,self.midiport)
    def update(self,delay):
        try:
            self.eventID.cancel();
            self.eventID = reactor.callLater(delay,noteOn,self.linecode,self.departureCode,self.stop,self.midiport)
        except twisted.internet.error.AlreadyCalled:
            del self

class Stop:
    def __init__(self,stopCode,stopName, nextCheck,x,y):
        self.stopCode = stopCode
        self.stopName = stopName
        self.nextCheck = nextCheck
        self.x = x
        self.y = y

global notelength
notelength = 1.000

def noteOff(i,midiport,stop):
    global note_lock
    note_lock[i] = 0
    midiport.send_message([0x80,i,0])
    x = stop.x
    y = stop.y
    screen.blit(carte, (x-10, y-10), pygame.Rect(x-10, y-10, 20, 20))
    pygame.display.flip()
    #departureList.pop(0)


def noteOn(linecode,i,stop,midiport):
    global next_scale_change
    global scale
    if next_scale_change < time.time():
        switch_scale()
        next_scale_change = time.time() + 3*60
    v = random.randint(40,70)
    while True:
        if linecode == '12' or linecode == '14' or linecode == '15' or linecode =='17' or linecode == '18':
            n = random.randint(0,int((len(scale)-1)/2))
        else:
            n = random.randint(int(len(scale)/2),len(scale)-1)
        if note_lock[scale[n]] == 0:
            note_lock[scale[n]] = 1
            break;
    print(linecode,(4-len(linecode))*' ',stop.stopName,(20-len(stop.stopName))*' ',notetab[n%12],(3-len(notetab[n%12]))*' ',v,(4-len(str(v)))*' ',i,(8-len(str(i)))*' ',sep="")
    color = colorList.get(linecode,(0,0,0))
    pygame.draw.circle(screen,color,(stop.x,stop.y),10)
    pygame.display.flip()
    midiport.send_message([0x90, scale[n], v])
    reactor.callLater(notelength,noteOff,scale[n],midiport,stop)


def find(f,seq):
    return  next((item for item in seq if item.departureCode == f),None)


def refresh(stoplist,midiout):
    next_scale_change = 0.0
    while refresh_running:
        midiout.send_message([0xB0,64,127])
        start = time.time()
        h1 = http.client.HTTPConnection('rtpi.data.tpg.ch',80,timeout = 10)
        i = 0
        for stop in stoplist:
            if len(stop.stopCode) == 4 and stop.nextCheck < time.time():
                i = i +1
                if refresh_running == False:
                    break;

                requestString = '/v1/GetNextDepartures.xml?key=90af88e0-c3d9-11e4-9114-0002a5d5c51b&stopCode=' + stop.stopCode
                try:
                    h1.request('GET',requestString)
                    r1 = h1.getresponse()
                    if r1.status != 200:
                        print('error getting data for stop '+ stop.stopCode)
                        return;
                    departuretree = xml.dom.minidom.parseString(r1.read().decode('utf-8'))
                    departurecol = departuretree.documentElement
                    departures = departurecol.getElementsByTagName('departure')
                    for departure in departures:
                        if len(departure.getElementsByTagName('waitingTimeMillis')) > 0:
                            departurecode = int(departure.getElementsByTagName('departureCode')[0].childNodes[0].data)
                            delay = float(departure.getElementsByTagName('waitingTimeMillis')[0].childNodes[0].data);
                            delay = delay/1000.0;
                            linecode = departure.getElementsByTagName('lineCode')[0].childNodes[0].data
                            if delay > 150 and departure == departures[0]:       # next arrival for this stop
                                stop.nextCheck = time.time()+delay*0.70         #check again at 0.8 * next arrival at this stop
                            elif delay > 0:
                                if delay < 150:
                                    at = find(departurecode,departureList)
                                    if at == None:
                                         new_stopped = BusStopped(linecode,departurecode,delay,stop,midiout);
                                         departureList.append(new_stopped);
                                    else:
                                        at.update(delay)
                        else :
                            if not (time.time() < stop.nextCheck < time.time()+3600): #si la prochaine verification est prevue avant 1h, on garde
                                stop.nextCheck = time.time()+(0.70*3600)
                except http.client.BadStatusLine:
                    print('BadStatusLine:',requestString)
                except http.client.CannotSendRequest:
                    print('CannotSendRequest:',requestString, r1.reason)
                except socket.timeout:
                    print('socket.timeout:',requestString, r1.reason)

        end = time.time()
        print('done in ',end-start,"seconds, for",i,"stops")
        h1.close()




notetab = ['C','C#','D','D#','E','F','F#','G','G#','A','A#','B']
midiout = rtmidi.MidiOut()
midiout.open_virtual_port("MusicalStop Midiport")
random.seed()


pygame.init()
screen = pygame.display.set_mode((dim[0],dim[1]))
carte = carte.convert()
screen.blit(carte,(0,0))
pygame.display.flip()
n = 0
print('get Stop list...')
h1 = http.client.HTTPConnection('rtpi.data.tpg.ch',80,timeout = 20);
h1.request('GET','/v1/GetLinesColors.xml?key=90af88e0-c3d9-11e4-9114-0002a5d5c51b')
r1 = h1.getresponse()
if r1.status == 200:
    data = r1.read()
    colortree = xml.dom.minidom.parseString(data.decode('utf-8'))
    colorcol = colortree.documentElement
    colors = colorcol.getElementsByTagName('color')
    for color in colors:
        linecode = color.getElementsByTagName('lineCode')[0].childNodes[0].data
        s = color.getElementsByTagName('hexa')[0].childNodes[0].data
        colortuple = (int(s[:2],16),int(s[2:4],16),int(s[4:],16))
        colorList[linecode] = colortuple
else:
    print('error getting the colorlines',r1.status, r1.reason)
colorList.setdefault((0,0,0))

while True:
    h1.request('GET','/v1/GetStops.xml?key=90af88e0-c3d9-11e4-9114-0002a5d5c51b');
    r1 = h1.getresponse()
    if r1.status == 200:
        print('parsing data')
        data = r1.read()
        stoptree = xml.dom.minidom.parseString(data.decode('utf-8'))
        stopcol = stoptree.documentElement
        stops = stopcol.getElementsByTagName('stop')
        stoplist = []
        for stop in stops:
            stopCode = stop.getElementsByTagName('stopCode')[0].childNodes[0].data
            stopName = stop.getElementsByTagName('stopName')[0].childNodes[0].data
            nextCheck = time.time()
            h1.request('GET','/v1/GetPhysicalStops.xml?key=90af88e0-c3d9-11e4-9114-0002a5d5c51b&stopCode='+stopCode)
            r1 = h1.getresponse()
            data = r1.read()
            phytree = xml.dom.minidom.parseString(data.decode('utf-8'))
            phycol = phytree.documentElement
            physicals = phycol.getElementsByTagName('physicalStop')
            geolat = []
            geolon = []
            for physical in physicals:
                physicalCode = physical.getElementsByTagName('physicalStopCode')[0].childNodes[0].data
                latitude = float(physical.getElementsByTagName('latitude')[0].childNodes[0].data)
                longitude = float(physical.getElementsByTagName('longitude')[0].childNodes[0].data)
                geolat.append(latitude)
                geolon.append(longitude)
            deltalat = coordinates[0][0]-coordinates[1][0]
            deltalon = coordinates[1][1]-coordinates[0][1]              
            if (coordinates[0][0] > latitude > coordinates[1][0]) and (coordinates[1][1] > longitude > coordinates[0][1]):
                n = n + 1
                coordy = ((latitude - coordinates[1][0])/deltalat)*850
                coordy = int(850 - coordy)
                coordx = int(((longitude - coordinates[0][1])/deltalon)*1440)

            sumlat = 0.0
            sumlon = 0.0
            for i in range(len(geolat)):
                sumlat = sumlat + geolat[i]
                sumlon = sumlon + geolon[i]

            sumlat = sumlat/(len(geolat))
            sumlon = sumlon/(len(geolon))
            # 46.226166, 6.078652
            # 46.176030, 6.184632
            #deltalat = 46.226166 - 46.176030
            #deltalon = 6.184632 - 6.078652
            deltalat = coordinates[0][0]-coordinates[1][0]
            deltalon = coordinates[1][1]-coordinates[0][1]
            #if (46.226166 > sumlat > 46.176030) and (6.184632 > sumlon > 6.078652):
            if (coordinates[0][0] > sumlat > coordinates[1][0]) and (coordinates[1][1] > sumlon > coordinates[0][1]):
                n = n + 1
                coordy = ((sumlat - coordinates[1][0])/deltalat)*dim[1] #46.176030)/deltalat)*850
                coordy = int(850 - coordy)
                coordx = int(((sumlon - coordinates[0][1])/deltalon)*dim[0])# 6.078652)/deltalon)*1440)
                stoplist.append(Stop(stopCode,stopName,nextCheck,coordx,coordy))
                print(n,stopCode, ":",coordx,coordy)

        print('done')
        midiout.send_message([0xB0,64,127])  #turn on sustain
        h1.close()
        break;
    else:
        print(r1.reason)
        time.sleep(2)
        #sys.exit(-1);

def quit_and_clean(signum, frame):
    reactor.callFromThread(reactor.stop)


signal.signal(signal.SIGINT, quit_and_clean)


departureList = []

refresh_running = True
t = threading.Thread(target=refresh, args=(stoplist,midiout))
t.setDaemon(True)
t.start()

def quit_verif():
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            reactor.callFromThread(reactor.stop)

l = task.LoopingCall(quit_verif)
l.start(1.0) # call every second


reactor.run()

refresh_running = False
t.join()
pygame.quit()
